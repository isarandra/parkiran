import Parkiran.Parkiran;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ParkiranTest {

    Parkiran parkiran;

    @BeforeEach
    void mulai(){
        parkiran=new Parkiran();
    }

    @Test
    void masuk_kendaraanBaruMasuk(){
        parkiran.masuk("motor","hitam putih","H 123 K",13,11);
        boolean kendaraanDiParkiran=parkiran.getListKendaraanDiParkiran().contains("motor hitam putih h 123 k");
        Assertions.assertEquals(true,kendaraanDiParkiran);
    }

//    @Test
//    void masuk_mobilBaruMasuk(){
//        parkiran.masuk("Mobil","Biru","B4567Y",12,10);
//        boolean kendaraanDiParkiran = parkiran.getListKendaraanDiParkiran().contains("mobil biru b4567y");
//        Assertions.assertEquals(true,kendaraanDiParkiran);
//    }

    @Test
    void masuk_kendaraanSudahDiParkiran(){
        parkiran.masuk("motor","putih","i11j",15,3);
        String hasilCode=parkiran.masuk("motor","putih","i11j",15,3);
        String ekspektasi="motor putih i11j sudah di parkiran";
        Assertions.assertEquals(ekspektasi,hasilCode);
    }

    @Test
    void masuk_inputNull(){
        String hasilCode=parkiran.masuk(null,null,null,0,0);
        String ekspektasi=null;
        Assertions.assertEquals(ekspektasi,hasilCode);
    }

    @Test
    void keluar_inputNull(){
        String hasilCode=parkiran.keluar(null,0,0);
        String ekspektasi=null;
        Assertions.assertEquals(ekspektasi,hasilCode);
    }

    @Test
    void keluar_kendaraanTidakAdaDiParkiran(){
        String hasilCode=parkiran.keluar("1233-kkkk-cccc",13,12);
        String ekspektasi=null;
        Assertions.assertEquals(ekspektasi,hasilCode);
    }

    @Test
    void keluar_kendaraanAdaDiParkiran(){
        parkiran.masuk("mobil","merah","A 1 A", 1,15);
        String idTiket=String.valueOf(parkiran.getListTiketDanKendaraanya().keySet().stream().toArray()[0]);
        String hasilCode=parkiran.keluar(idTiket,1,20);
        String ekspektasi="mobil merah a 1 a telah keluar parkiran";
        Assertions.assertEquals(ekspektasi,hasilCode);
    }

    @Test
    void kalkulasiProfit_tidakAdaYangMasukdanKeluar(){
        int hasilCode=parkiran.getProfit();
        int ekspektasi=0;
        Assertions.assertEquals(ekspektasi,hasilCode);
    }

    @Test
    void kalkulasiProfit_motorParkirKurangDariSejam(){
        parkiran.masuk("Motor","Kuning","B123K",6,5);
        String idTiket=String.valueOf(parkiran.getListTiketDanKendaraanya().keySet().stream().toArray()[0]);
        parkiran.keluar(idTiket,6,5);
        int hasilCode=parkiran.getProfit();
        int ekspektasi=1500;
        Assertions.assertEquals(ekspektasi,hasilCode);
    }

    @Test
    void kalkulasiProfit_motorParkirSejam(){
        parkiran.masuk("motor","emas","XX 12 12 3 Y",7,15);
        String idTiket=String.valueOf(parkiran.getListTiketDanKendaraanya().keySet().stream().toArray()[0]);
        parkiran.keluar(idTiket,7,16);
        int hasilCode=parkiran.getProfit();
        int ekspektasi=1500;
        Assertions.assertEquals(ekspektasi,hasilCode);
    }

    @Test
    void kalkulasiProfit_motorParkirDelapanJam(){
        parkiran.masuk("mOtOr","JiNgGa","zzzz",1,9);
        String idTiket=String.valueOf(parkiran.getListTiketDanKendaraanya().keySet().stream().toArray()[0]);
        parkiran.keluar(idTiket,9,17);
        int hasilCode=parkiran.getProfit();
        int ekspektasi=12000;
        Assertions.assertEquals(ekspektasi,hasilCode);
    }

    @Test
    void kalkulasiProfit_mobilParkirKurangDariSejam(){
        parkiran.masuk("mobil","abu abu","zz xx cc",11,2);
        String idTiket=String.valueOf(parkiran.getListTiketDanKendaraanya().keySet().stream().toArray()[0]);
        parkiran.keluar(idTiket,11,2);
        int hasilCode=parkiran.getProfit();
        int ekspektasi=5000;
        Assertions.assertEquals(ekspektasi,hasilCode);
    }

    @Test
    void kalkulasiProfit_mobilParkirSejam(){
        parkiran.masuk("mobil","transparan","H 2358 YU",13,13);
        String idTiket=String.valueOf(parkiran.getListTiketDanKendaraanya().keySet().stream().toArray()[0]);
        parkiran.keluar(idTiket,13,14);
        int hasilCode=parkiran.getProfit();
        int ekspektasi=5000;
        Assertions.assertEquals(ekspektasi,hasilCode);
    }

    @Test
    void kalkulasiProfit_mobilParkirSebelasJam(){
        parkiran.masuk("MoBiL","PiiNk","alay beut",3,1);
        String idTiket=String.valueOf(parkiran.getListTiketDanKendaraanya().keySet().stream().toArray()[0]);
        parkiran.keluar(idTiket,3,12);
        int hasilCode=parkiran.getProfit();
        int ekspektasi=25000;
        Assertions.assertEquals(ekspektasi,hasilCode);
    }

    @Test
    void meanWaktuParkir_tidakAdaYangParkir(){
//        List<Double> meanWaktuParkir(List<Integer> listWaktuParkir);
        List<Double> hasilCode=parkiran.meanWaktuParkir(parkiran.getListWaktuParkir());
        List<Double> ekspektasi= new ArrayList<>();
        ekspektasi.add(0,0d);
        Object[] hasilCodeArray=hasilCode.toArray();
        Object[] ekspektasiArray=ekspektasi.toArray();
        Assertions.assertArrayEquals(ekspektasiArray,hasilCodeArray);
    }

    @Test
    void meanWaktuParkir_SatuKendaraanParkir5Jam(){
//        List<Double> meanWaktuParkir(List<Integer> listWaktuParkir);
        parkiran.masuk("mobil","biru","LK 13 K",11,12);
        String idTiket=String.valueOf(parkiran.getListTiketDanKendaraanya().keySet().stream().toArray()[0]);
        parkiran.keluar(idTiket,11,17);
        List<Double> hasilCode=parkiran.meanWaktuParkir(parkiran.getListWaktuParkir());
        List<Double> ekspektasi= new ArrayList<>();
        ekspektasi.add(0,5d);
        Object[] hasilCodeArray=hasilCode.toArray();
        Object[] ekspektasiArray=ekspektasi.toArray();
        Assertions.assertArrayEquals(ekspektasiArray,hasilCodeArray);
    }

    @Test
    void meanWaktuParkir_tigaKendaraan(){
//        List<Double> meanWaktuParkir(List<Integer> listWaktuParkir);
        parkiran.masuk("motor","putih","A 1 A",13,2);
        String idTiket=String.valueOf(parkiran.getListTiketDanKendaraanya().keySet().stream().toArray()[0]);
        parkiran.keluar(idTiket,13,5);
        parkiran.masuk("mobil","merah","B 2 B",13,9);
        String idTiket2=String.valueOf(parkiran.getListTiketDanKendaraanya().keySet().stream().toArray()[0]);
        parkiran.keluar(idTiket2,13,18);
        parkiran.masuk("mobil","jingga","C 3 C",13,7);
        String idTiket3=String.valueOf(parkiran.getListTiketDanKendaraanya().keySet().stream().toArray()[0]);
        parkiran.keluar(idTiket3,13,12);
        List<Double> hasilCode=parkiran.meanWaktuParkir(parkiran.getListWaktuParkir());
        List<Double> ekspektasi= new ArrayList<>();
        ekspektasi.add(0,17d/3);
        Object[] hasilCodeArray=hasilCode.toArray();
        Object[] ekspektasiArray=ekspektasi.toArray();
        Assertions.assertArrayEquals(ekspektasiArray,hasilCodeArray);
    }
}
