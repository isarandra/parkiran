package Parkiran;

import java.util.List;
import java.util.UUID;

public interface IParkiran {

    String masuk(String jenisKendaraan,String warna,String platNomor,int tanggalMasuk,int jamMasuk);

    String keluar(String idTiket,int tanggalKeluar, int jamKeluar);

    int kalkulasiProfit(UUID idTiketUUID, int jamKeluar);

    List<Double> meanWaktuParkir(List<Integer> listWaktuParkir);

}
