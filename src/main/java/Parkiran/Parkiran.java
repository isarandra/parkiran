package Parkiran;

import java.util.*;
import java.util.stream.Collectors;

public class Parkiran implements IParkiran{
    private int profit;

    public List<Integer> getListWaktuParkir() {
        return listWaktuParkir;
    }

    private List<Integer> listWaktuParkir=new ArrayList<>();

    public int getProfit() {
        return profit;
    }

    public void setProfit(int profit) {
        this.profit = profit;
    }

    private Map<UUID,List<String>> listTiketDanKendaraannya = new HashMap<>(); //key:random-id    value:kendaraan

    private List<String> listKendaraanDiParkiran=new ArrayList<>(); // kendaraan yang ada di parkiran

    public Map<UUID, List<String>> getListTiketDanKendaraanya() {
        return listTiketDanKendaraannya;
    }

    public List<String> getListKendaraanDiParkiran() {
        return listKendaraanDiParkiran;
    }

    @Override
    public String masuk(String jenisKendaraan, String warna, String platNomor, int tanggalMasuk, int jamMasuk) {
        try{
            String identitasKendaraan = jenisKendaraan.trim().toLowerCase() +" "
                    +warna.trim().toLowerCase()+" "
                    +platNomor.trim().toLowerCase();
            if(listKendaraanDiParkiran.contains(identitasKendaraan)){
    //            System.out.println(jenisKendaraan+" "+warna+" plat "+platNomor+" alias kendaraan lo udah di parkiran bang");
                System.out.println(identitasKendaraan+" sudah di parkiran");
                System.out.println();
                return identitasKendaraan+" sudah di parkiran";
            }
            else{
                UUID uuid = UUID.randomUUID();
                listKendaraanDiParkiran.add(identitasKendaraan);
                List<String> kendaraan = new ArrayList<>(); // jenis kendaraan warna plat nomor, tanggal masuk, jam masuk
                String motor ="";
                if(jenisKendaraan.equalsIgnoreCase("motor")){
                    motor=String.valueOf(0);
                }
                else{
                    motor=String.valueOf(1);
                }
                kendaraan.add(0,identitasKendaraan);
                kendaraan.add(1,motor); // 0 untuk motor, 1 untuk mobil
                kendaraan.add(2,String.valueOf(tanggalMasuk));
                kendaraan.add(3,String.valueOf(jamMasuk));
                listTiketDanKendaraannya.put(uuid,kendaraan);
    //            System.out.println(jenisKendaraan+" "+warna+" plat "+platNomor+" masuk tanggal "+tanggalMasuk+" jam "+jamMasuk);
    //            System.out.println("id tiket parkir lo :"+uuid);
                System.out.print("Tiket parkir "+identitasKendaraan+": ");
                System.out.println(uuid);
                System.out.println();
                return "";
    //            System.out.println(getListTiketDanKendaraanya());
            }
        }
        catch (NullPointerException nullPointerException){
            System.out.println("Input tidak boleh null");
            System.out.println();
            return null;
        }
    }

    @Override
    public String keluar(String idTiket, int tanggalKeluar, int jamKeluar) {
        try{
            UUID idTiketUUID = UUID.fromString(idTiket);
            if(listTiketDanKendaraannya.containsKey(idTiketUUID)){
                int biayaParkir=kalkulasiProfit(idTiketUUID,jamKeluar);
                System.out.println("Biaya parkir: Rp"+biayaParkir);
                String namaKendaraanYangKeluar=listTiketDanKendaraannya.get(idTiketUUID).get(0);
                System.out.println(namaKendaraanYangKeluar+" telah keluar parkiran");
                System.out.println();
                listKendaraanDiParkiran.remove(listTiketDanKendaraannya.get(idTiketUUID).get(0));
                listTiketDanKendaraannya.remove(idTiketUUID);
                return namaKendaraanYangKeluar+" telah keluar parkiran";
            }
            else{
                System.out.println("Tiket tidak diketahui");
                return null;
            }
        }
        catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Tiket tidak diketahui");
            System.out.println();
            return null;
        }
        catch (NullPointerException nullPointerException){
            System.out.println("Input tidak boleh null");
            System.out.println();
            return null;
        }
    }

    @Override
    public int kalkulasiProfit(UUID idTiketUUID,int jamKeluar) {
        int biayaParkir=0;
        int biayaDasarMotor=1500;
        int biayaPerJamMotor=1500;
        int biayaDasarMobil=5000;
        int biayaPerJamMobil=2000;
        int jamMasuk= Integer.valueOf(listTiketDanKendaraannya.get(idTiketUUID).get(3));
        boolean isMotor=false;
        if(Integer.valueOf(listTiketDanKendaraannya.get(idTiketUUID).get(1))==0){
            isMotor=true;
        }
        if(jamKeluar-jamMasuk==0 && isMotor){
            biayaParkir+=biayaDasarMotor;
        }
        else if(jamKeluar-jamMasuk>0 && isMotor){
            biayaParkir+=biayaDasarMotor+(jamKeluar-jamMasuk-1)*biayaPerJamMotor;
        }
        else if(jamKeluar-jamMasuk==0 && !isMotor){
            biayaParkir+=biayaDasarMobil;
        }
        else if(jamKeluar-jamMasuk>0 && !isMotor){
            biayaParkir+=biayaDasarMobil+(jamKeluar-jamMasuk-1)*biayaPerJamMobil;
        }
        profit+=biayaParkir;
        listWaktuParkir.add(jamKeluar-jamMasuk);
        return biayaParkir;
    }

    @Override
    public List<Double> meanWaktuParkir(List<Integer> listWaktuParkir) {
        List<Double> output= new ArrayList<>();
        Double mean=listWaktuParkir.stream().collect(Collectors.summarizingInt(Integer::valueOf)).getAverage();
        output.add(mean);
        return output;
    }
}
