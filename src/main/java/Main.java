import Parkiran.Parkiran;

import java.util.List;
import java.util.Scanner;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        System.out.println("--------------------");
        System.out.println("Aplikasi Kang Parkir");
        System.out.println("--------------------");
        System.out.println();

        Parkiran parkiran = new Parkiran();

//        parkiran.masuk("mobil","jingga","H 423 K",13,9);
//        parkiran.masuk("mobil","jingga","H 423 K",13,9); //should fail
//        parkiran.keluar("mobil","jingga","H 423 K",13,15);    biaya: 5000+10000=15.000
//        parkiran.masuk("motor","biru","AB879ZT",13,12);
//        parkiran.masuk("motor","abu abu","KZ 22 Y",13,9);
//        parkiran.keluar("motor","abu abu","KZ 22 Y",13,11);    biaya: 3.000
//        parkiran.masuk("mobil","kuning","A 1 X", 13, 8);
//        parkiran.keluar("motor","biru","AB879ZT",13,18); biaya: 1500+7500=9.000
//        parkiran.keluar("mobil","kuning","A 1 X", 13, 9); biaya: 5.000
//        profit=32.000

        while(true){
            System.out.println("Menu: ");
            System.out.println("1. Masuk parkiran");
            System.out.println("2. Keluar parkiran");
            System.out.println("3. Daftar kendaraan di parkiran");
            System.out.println("4. Keuntungan hari ini");
            System.out.println("5. Rata-rata waktu parkir");
            System.out.println("0. Stop program");
            Scanner scanner = new Scanner(System.in);
            int controlMenuKeluarMasuk=scanner.nextInt();
            System.out.println();
            switch (controlMenuKeluarMasuk){
                case 1:
                    scanner.nextLine();
                    System.out.print("Jenis kendaraan (motor/mobil): ");
                    String jenisKendaraan= scanner.nextLine();
                    if(jenisKendaraan.equalsIgnoreCase("mobil") ||
                            jenisKendaraan.equalsIgnoreCase("motor")){
                        System.out.print("Warna kendaraan: ");
                        String warna=scanner.nextLine();
                        System.out.print("Plat nomor: ");
                        String platNomor=scanner.nextLine();
                        System.out.print("Tanggal masuk (cth: 13): ");
                        int tanggalMasuk=scanner.nextInt();
                        System.out.print("Jam masuk (cth: 22): ");
                        int jamMasuk=scanner.nextInt();
//                    System.out.println(jenisKendaraan);
//                    System.out.println(warna);
//                    System.out.println(platNomor);
//                    System.out.println(tanggalMasuk);
//                    System.out.println(jamMasuk);
                        System.out.println();
                        parkiran.masuk(jenisKendaraan,warna,platNomor,tanggalMasuk,jamMasuk);
                        break;
                    }
                    else{
                        System.out.println();
                        System.out.println("Jenis kendaraan harus motor atau mobil, tanpa spasi");
                        System.out.println();
                        continue;
                    }
                case 2:
                    System.out.print("ID tiket (tanpa spasi): ");
                    String idTiket= scanner.next();
                    System.out.print("Tanggal keluar (cth: 13): ");
                    int tanggalKeluar=scanner.nextInt();
                    System.out.print("Jam keluar (cth: 23): ");
                    int jamKeluar=scanner.nextInt();
                    System.out.println();
//                    UUID idTiketUUID=UUID.fromString(idTiket);
//                    System.out.println(parkiran.getListTiketDanKendaraanya().get(idTiketUUID));
                    parkiran.keluar(idTiket,tanggalKeluar,jamKeluar);
                    break;
                case 3:
                    if(parkiran.getListKendaraanDiParkiran().isEmpty()){
                        System.out.println("Tidak ada kendaraan di parkiran");
                        System.out.println();
                    }
                    else{
                        System.out.println(parkiran.getListKendaraanDiParkiran());
                        System.out.println();
                    }
                    break;
                case 4:
                    System.out.print("Keuntungan hari ini: Rp"+parkiran.getProfit());
                    System.out.println();
                    System.out.println();
                    break;
                case 5:
                    List<Double> meanWaktuParkir=parkiran.meanWaktuParkir(parkiran.getListWaktuParkir());
                    System.out.println("Rata-rata waktu parkir: "+meanWaktuParkir.get(0)+" jam");
                    System.out.println();
                    break;
                case 0:
                    System.exit(1);
            }
        }



    }

}
